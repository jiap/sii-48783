#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

int main (void){
int fifo1;  
char buf[1024];


mkfifo("/tmp/logger",0666);
fifo1=open("/tmp/logger",O_RDONLY);

if (fifo1<0)
{
printf("error al abrir el fifo \n");
return 1;
}
while(1) {
if(read(fifo1,buf,sizeof(buf))<0)
{
printf("error al leer el fifo \n");
break;
}
printf("%s",buf);
if(strcmp(buf,"El programa ha terminado \n")==0)
break;
}
close(fifo1);
unlink("tmp/logger");
return 0;
}
